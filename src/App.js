import * as React from "react";
import { Routes, Route } from "react-router-dom";
import "./App.css";
import BookDetail from "./components/containers/BookDetail";
import Books from "./components/containers/Books";
import Home from "./components/containers/Home";
import StudentDetail from "./components/containers/StudentDetail";
import Students from "./components/containers/Students";
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export default function App() {
  const [open, setOpen] = React.useState(false);
  const [message, setMessage] = React.useState("");
  const [severity, setSeverity] = React.useState("");

  const openSnackBar = (message, severity) => {
    setSeverity(severity);
    setMessage(message);
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    
    setMessage("");
    setOpen(false);
  };
  return (
    <div className="App">
      <Routes>
        <Route exact path="/" element={<Home openSnackBar={openSnackBar}/>} />
        <Route exact path="/students" element={<Students openSnackBar={openSnackBar}/>} />
        <Route path="/students/:id" element={<StudentDetail openSnackBar={openSnackBar}/>}/>
        <Route exact path="/books" element={<Books openSnackBar={openSnackBar}/>}/>
        <Route path="/book/:id" element={<BookDetail openSnackBar={openSnackBar}/>}/>
      </Routes>
      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={severity} sx={{ width: '100%' }}>
          {message}
        </Alert>
      </Snackbar>
    </div>
  );
}


