import React from "react";
import StudentsView from "../views/Students";
import axios from "axios";

export default function Students(props) {
    const [formState, setForm] = React.useState({
        firstName: "",
        lastName: ""
    });
    const [students, setStudents] = React.useState([]);

    const getStudents = async function () {
        try {
            const response = await axios.get("http://localhost:8080/students");
            setStudents(response.data.students);
        }
        catch (err) {
            props.openSnackBar("Something went wrong, unable to fetch data.", "error");
        }
    }

    const fieldChange = function (event) {
        setForm({ ...formState, [event.target.name]: event.target.value });
    }

    const onSubmit = async function () {
        if (formState.firstName && formState.lastName) {
            try {
                await axios.post("http://localhost:8080/students", { ...formState });
                const response = await axios.get("http://localhost:8080/students");
                setStudents(response.data.students);
                props.openSnackBar("Student Successfully Added", "success");
            }
            catch (err) {
                props.openSnackBar("Something went wrong, try again.", "error");
            }
        }
    }
    React.useEffect(() => {
        getStudents();
    }, []);
    return <StudentsView formState={formState} students={students} fieldChange={fieldChange} onSubmit={onSubmit}/>
}