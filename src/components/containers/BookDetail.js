import React from "react";
import BookDetailView from "../views/BookDetail";
import axios from "axios";
import { useParams } from "react-router";
import { useNavigate } from "react-router";
import { BOOKS, BORROW_BOOK } from "./apiEndpoints";

export default function BookDetail(props) {
    const { id } = useParams();
    const [book, setBook] = React.useState(null);
    const [formState, setForm] = React.useState({
        bookName: "",
        author: ""
    });

    const navigate = useNavigate();

    const [borrowerFormState, setBorrowerForm] = React.useState({
        studentId: "",
        expectedDateOfReturn: ""
    });

    const borrowerFieldChange = function (event) {
        setBorrowerForm({ ...borrowerFormState, [event.target.name]: event.target.value });
    }

    const fieldChange = function (event) {
        setForm({ ...formState, [event.target.name]: event.target.value });
    }

    const getBook = async function () {
        try {
            const response = await axios.get(`${BOOKS}${id}`);
            setBook(response.data.books[0]);
            setForm({ bookName: response.data.books[0].bookName, author: response.data.books[0].author });
        }
        catch (err) {
            props.openSnackBar("Something went wrong, unable to fetch data.", "error");
        }
    };

    const onSubmit = async function () {
        if (formState.bookName && formState.author) {
            try {
                await axios.put(`${BOOKS}${id}`, { ...formState });
                const response = await axios.get(`${BOOKS}${id}`);
                setBook(response.data.books[0]);
                props.openSnackBar("Book Successfully Updated!", "success");
            }
            catch (err) {
                props.openSnackBar("Something went wrong, try again.", "error");
            }
        }
    }

    const onDelete = async function () {
        try {
            await axios.delete(`${BOOKS}${id}`);
            navigate('/books');
            props.openSnackBar("Book Successfully Deleted!", "success");
        }
        catch (err) {
            props.openSnackBar("Something went wrong, try again.", "error");
        }
    }

    const borrowerOnSubmit = async function () {
        if (borrowerFormState.studentId && borrowerFormState.expectedDateOfReturn) {
            try {
                await axios.post(`${BORROW_BOOK}${id}`, { ...borrowerFormState });
                const response = await axios.get(`${BOOKS}${id}`);
                setBook(response.data.books[0]);
                props.openSnackBar("Borrower Successfully Added!", "success");
            }
            catch (err) {
                props.openSnackBar("Something went wrong, try again.", "error");
            }
        }
    }

    const removeBorrower = async function () {
        try {
            await axios.delete(`${BORROW_BOOK}${id}`);
            const response = await axios.get(`${BOOKS}${id}`);
            setBook(response.data.books[0]);
            props.openSnackBar("Borrower Successfully Removed!", "success");
        }
        catch (err) {
            props.openSnackBar("Something went wrong, try again.", "error");
        }
    }

    React.useEffect(async () => {
        getBook();
    }, []);

    return <BookDetailView onDelete={onDelete} removeBorrower={removeBorrower} borrowerOnSubmit={borrowerOnSubmit} borrowerFormState={borrowerFormState} borrowerFieldChange={borrowerFieldChange} formState={formState} book={book} fieldChange={fieldChange} onSubmit={onSubmit} />
}