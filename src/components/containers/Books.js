import React from "react";
import BooksView from "../views/Books";
import axios from "axios";
import { BOOKS } from "./apiEndpoints";

export default function Books(props){
    const [formState, setForm] = React.useState({
       bookName: "",
       author: ""
    });
    const [books, setBooks] = React.useState([]);

    const getBooks = async function () {
        try{
            const response = await axios.get(BOOKS);
            setBooks(response.data.books);
        }
        catch(err){
            props.openSnackBar("Something went wrong, unable to fetch data.", "error");
        }
    }

    const fieldChange = function (event){
       setForm({...formState, [event.target.name]: event.target.value});
    }

    const onSubmit = async function (){
        if(formState.bookName && formState.author){
            try {
               await axios.post(BOOKS, {...formState});
               const response = await axios.get(BOOKS);
               setBooks(response.data.books);
               props.openSnackBar("Book Successfully Added", "success");
            }
            catch(err){
                props.openSnackBar("Something went wrong, try again.", "error");
            }
        }
    }
    React.useEffect(()=>{
        getBooks();
      },[]);
    return <BooksView formState={formState} books={books} onSubmit={onSubmit} fieldChange={fieldChange} />
}