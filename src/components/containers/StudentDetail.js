import React from "react";
import axios from "axios";
import { useNavigate, useParams } from "react-router";
import StudentDetailView from "../views/StudentDetail";
import { STUDENTS } from "./apiEndpoints";

export default function StudentDetail(props) {
    const { id } = useParams();
    const [student, setStudent] = React.useState(null);
    const [formState, setForm] = React.useState({
        firstName: "",
        lastName: ""
    });
    const navigate = useNavigate();

    const getStudent = async function () {
        try {
            const response = await axios.get(`${STUDENTS}${id}`);
            setStudent(response.data.students[0]);
            setForm({ firstName: response.data.students[0].firstName, lastName: response.data.students[0].lastName });
        }
        catch (err) {
            props.openSnackBar("Something went wrong, unable to fetch data.", "error");
        }
    };

    const fieldChange = function (event) {
        setForm({ ...formState, [event.target.name]: event.target.value });
    };

    const onSubmit = async function () {
        if (formState.firstName && formState.lastName) {
            try {
                await axios.put(`${STUDENTS}${id}`, { ...formState });
                const response = await axios.get(`${STUDENTS}${id}`);
                setStudent(response.data.students[0]);
                props.openSnackBar("Student Successfully Updated!", "success");
            }
            catch (err) {
                props.openSnackBar("Something went wrong, try again.", "error");
            }
        }
    };

    const onDelete = async function () {
        try {
            await axios.delete(`${STUDENTS}${id}`);
            navigate('/students');
            props.openSnackBar("Student Successfully Deleted!", "success");
        }
        catch (err) {
            props.openSnackBar("Something went wrong, try again.", "error");
        }
    };

    React.useEffect(async () => {
        getStudent();
    }, []);

    return <StudentDetailView onDelete={onDelete} onSubmit={onSubmit} student={student} formState={formState} fieldChange={fieldChange} />
}