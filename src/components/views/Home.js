import { Typography } from "@mui/material";
import { Box } from "@mui/system";
import { Link } from "react-router-dom";

export default function HomeView(props) {
    return (
      <Box>
        <main>
          <Typography variant="h2" component="h2">Welcome to the Books Library Management Service</Typography>
        </main>
        <Box className="nav">
          <Link to="/students">Students</Link>
          <Link to="/books">Books</Link>
        </Box>
      </Box>
    );
  }

