import React from "react";
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import FormControl from '@mui/material/FormControl';
import Typography from '@mui/material/Typography';
import InputLabel from '@mui/material/InputLabel';
import Input from '@mui/material/Input';
import Modal from '@mui/material/Modal';
import EditOutlined from '@mui/icons-material/EditOutlined';
import FormGroup from '@mui/material/FormGroup';

export default function StudentDetailView(props){
    const { fieldChange, onSubmit, onDelete, student, formState } = props;
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const formSubmit = () => {
        handleClose();
        onSubmit();
    };
    return (<Box>
        <Typography id="modal-modal-title" variant="h3" component="h3">Student Details</Typography>
        {student &&
            <Box className="book-container">
                <Box onClick={handleOpen} className="edit-icon-container">
                    <EditOutlined />
                </Box>
                <Box className="rows">
                    <Box className="book-detail-header">
                        <Typography variant="p" component="p">
                            First Name
                        </Typography>
                    </Box>
                    <Box className="book-detail-row">
                        <Typography variant="p" component="p">
                            {student.firstName}
                        </Typography>
                    </Box>
                </Box>
                <Box className="rows">
                    <Box className="book-detail-header">
                        <Typography variant="p" component="p">
                            Last Name
                        </Typography>
                    </Box>
                    <Box className="book-detail-row">
                        <Typography variant="p" component="p">
                            {student.lastName}
                        </Typography>
                    </Box>
                </Box>
                <Box className="rows btns-rows">
                    <Button onClick={onDelete} className="submit-form" variant="contained" color="error">
                        Delete Student
                    </Button>
                </Box>
            </Box>
        }
                <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box className="modal-container">
                <Typography id="modal-modal-title" variant="h6" component="h2">
                    Update Student
                </Typography>
                <Box className="form-container">
                    <FormGroup>
                    <FormControl>
                        <InputLabel variant="outlined" disableAnimation={true} htmlFor="my-input">First Name</InputLabel>
                        <Input value={formState.firstName} onChange={fieldChange} name="firstName" id="my-input" aria-describedby="my-helper-text" />
                    </FormControl>
                    </FormGroup>
                    <FormGroup>
                    <FormControl>
                        <InputLabel variant="outlined" disableAnimation={true} htmlFor="my-input">Last Name</InputLabel>
                        <Input value={formState.lastName} onChange={fieldChange} name="lastName" id="my-input" aria-describedby="my-helper-text" />
                    </FormControl>
                    </FormGroup>
                    <Button disabled={!formState.firstName || !formState.lastName} onClick={formSubmit} className="submit-form" variant="outlined">
                        Update
                    </Button>
                </Box>
            </Box>
        </Modal>
        </Box>);
}