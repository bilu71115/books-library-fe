import React from "react";
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import FormControl from '@mui/material/FormControl';
import Typography from '@mui/material/Typography';
import InputLabel from '@mui/material/InputLabel';
import Input from '@mui/material/Input';
import Modal from '@mui/material/Modal';
import EditOutlined from '@mui/icons-material/EditOutlined';
import FormGroup from '@mui/material/FormGroup';

export default function BookDetailView(props) {
    const { fieldChange, onSubmit, book, onDelete, borrowerOnSubmit, removeBorrower, formState, borrowerFormState, borrowerFieldChange } = props;
    const [open, setOpen] = React.useState(false);
    const [openBorrower, setBorrowerOpen] = React.useState(false);
    const handleBorrowerOpen = () => setBorrowerOpen(true);
    const handleBorrowerClose = () => setBorrowerOpen(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const formSubmit = () => {
        handleClose();
        onSubmit();
    };
    const borrowerFormSubmit = () => {
        handleBorrowerClose();
        borrowerOnSubmit();
    };

    return (<Box>
        <Typography id="modal-modal-title" variant="h3" component="h3">Book Details</Typography>
        {book &&
            <Box className="book-container">
                <Box onClick={handleOpen} className="edit-icon-container">
                    <EditOutlined />
                </Box>
                <Box className="rows">
                    <Box className="book-detail-header">
                        <Typography variant="p" component="p">
                            Book Name
                        </Typography>
                    </Box>
                    <Box className="book-detail-row">
                        <Typography variant="p" component="p">
                            {book.bookName}
                        </Typography>
                    </Box>
                </Box>
                <Box className="rows">
                    <Box className="book-detail-header">
                        <Typography variant="p" component="p">
                            Author
                        </Typography>
                    </Box>
                    <Box className="book-detail-row">
                        <Typography variant="p" component="p">
                            {book.author}
                        </Typography>
                    </Box>
                </Box>
                <Box className="rows">
                    <Box className="book-detail-header">
                        <Typography variant="p" component="p">
                            Borrowed By
                        </Typography>
                    </Box>
                    <Box className="book-detail-row">
                        <Typography variant="p" component="p">
                            {(book.firstName || "") + " " + (book.lastName || "")}
                        </Typography>
                    </Box>
                </Box>
                <Box className="rows">
                    <Box className="book-detail-header">
                        <Typography variant="p" component="p">
                            Borrowed on
                        </Typography>
                    </Box>
                    <Box className="book-detail-row">
                        <Typography variant="p" component="p">
                            {book.dateOfBorrow}
                        </Typography>
                    </Box>
                </Box>
                <Box className="rows">
                    <Box className="book-detail-header">
                        <Typography variant="p" component="p">
                            Expected date of return
                        </Typography>
                    </Box>
                    <Box className="book-detail-row">
                        <Typography variant="p" component="p">
                            {book.expectedDateOfReturn}
                        </Typography>
                    </Box>
                </Box>
                <Box className="rows btns-rows">
                    <Button onClick={onDelete} className="submit-form" variant="contained" color="error">
                        Delete Book
                    </Button>
                    <Button disabled={!book.firstName} onClick={removeBorrower} className="submit-form" variant="contained" color="error">
                        Remove Borrower
                    </Button>
                    <Button disabled={book.firstName} onClick={handleBorrowerOpen} className="submit-form" variant="contained">
                        Add Borrower
                    </Button>
                </Box>
            </Box>
        }
        <Modal
            open={openBorrower}
            onClose={handleBorrowerClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box className="modal-container">
                <Typography id="modal-modal-title" variant="h6" component="h2">
                    Assign a Student
                </Typography>
                <Box className="form-container">
                    <FormGroup>
                    <FormControl>
                        <InputLabel variant="outlined" disableAnimation={true} htmlFor="my-input">Student Id</InputLabel>
                        <Input value={borrowerFormState.studentId} onChange={borrowerFieldChange} name="studentId"  aria-describedby="my-helper-text" />
                    </FormControl>
                    </FormGroup>
                    <FormGroup>
                    <FormControl>
                        <InputLabel variant="outlined" disableAnimation={true} htmlFor="my-input">Expected to be returned</InputLabel>
                        <Input value={borrowerFormState.expectedDateOfReturn} type="date" onChange={borrowerFieldChange} name="expectedDateOfReturn" aria-describedby="my-helper-text" />
                    </FormControl>
                    </FormGroup>
                    <Button disabled={!borrowerFormState.studentId || !borrowerFormState.expectedDateOfReturn} onClick={borrowerFormSubmit} className="submit-form" variant="contained">
                        Add
                    </Button>
                </Box>
            </Box>
        </Modal>
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box className="modal-container">
                <Typography id="modal-modal-title" variant="h6" component="h2">
                    Update Book
                </Typography>
                <Box className="form-container">
                    <FormGroup>
                    <FormControl>
                        <InputLabel variant="outlined" disableAnimation={true} htmlFor="my-input">Book Name</InputLabel>
                        <Input value={formState.bookName} onChange={fieldChange} name="bookName" id="my-input" aria-describedby="my-helper-text" />
                    </FormControl>
                    </FormGroup>
                    <FormGroup>
                    <FormControl>
                        <InputLabel variant="outlined" disableAnimation={true} htmlFor="my-input">Author</InputLabel>
                        <Input value={formState.author} onChange={fieldChange} name="author" id="my-input" aria-describedby="my-helper-text" />
                    </FormControl>
                    </FormGroup>
                    <Button onClick={formSubmit} className="submit-form" variant="contained">
                        Update
                    </Button>
                </Box>
            </Box>
        </Modal>
    </Box>);
}