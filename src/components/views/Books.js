import React from "react";
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import FormControl from '@mui/material/FormControl';
import Typography from '@mui/material/Typography';
import InputLabel from '@mui/material/InputLabel';
import Input from '@mui/material/Input';
import Modal from '@mui/material/Modal';
import { DataGrid } from '@mui/x-data-grid';
import { useNavigate } from "react-router-dom";
import EditOutlined from '@mui/icons-material/EditOutlined';

const columns = [
  { field: 'bookName', headerName: 'Book Name', width: 150 },
  { field: 'author', headerName: 'Author', width: 150 },
  { field: 'firstName', headerName: 'Borrowed By', width: 150 },
  { field: 'dateOfBorrow', headerName: 'Date Of Borrow', width: 150 },
  { field: 'expectedDateOfReturn', headerName: 'Expected Date Of Return', width: 150 },
  {
    field: "actions",
    headerName: "Actions",
    sortable: false,
    width: 140,
    disableClickEventBubbling: true,
    renderCell: (params) => {
      return (
        <Box className="d-flex justify-content-between align-items-center" style={{ cursor: "pointer" }}>
          <EditOutlined style={{ fontSize: 16, backgroundColor: "rgb(79 79 79)" }} />
        </Box>
      );
    }
  }
];



export default function BooksView(props) {
  const { fieldChange, onSubmit, books, formState } = props;
  const navigate = useNavigate();
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const formSubmit = () => {
    handleClose();
    onSubmit();
  }

  return (
    <Box sx={{ height: 400, maxWidth: '1000px', margin: 'auto' }}>
      <Typography id="modal-modal-title" variant="h1" component="h1">Books</Typography>
      <Button onClick={handleOpen} variant="outlined" color="success" className="add-btn">Add New Book</Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box className="modal-container">
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Add New Book
          </Typography>
          <Box className="form-container">
            <FormControl>
              <InputLabel htmlFor="my-input">Book Name</InputLabel>
              <Input onChange={fieldChange} name="bookName" id="my-input" aria-describedby="my-helper-text" />
            </FormControl>
            <FormControl>
              <InputLabel htmlFor="my-input">Author</InputLabel>
              <Input onChange={fieldChange} name="author" id="my-input" aria-describedby="my-helper-text" />
            </FormControl>
            <Button disabled={!formState.bookName || !formState.author} onClick={formSubmit} className="submit-form" variant="contained">
              Add
            </Button>
          </Box>
        </Box>
      </Modal>
      <DataGrid
        onRowClick={(params) => navigate(`/book/${params.id}`)}
        style={{ "cursor": "pointer" }}
        rows={books}
        columns={columns}
      />
    </Box>
  );
}