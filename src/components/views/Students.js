import React from "react";
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import FormControl from '@mui/material/FormControl';
import Typography from '@mui/material/Typography';
import InputLabel from '@mui/material/InputLabel';
import Input from '@mui/material/Input';
import Modal from '@mui/material/Modal';
import { DataGrid } from '@mui/x-data-grid';
import { useNavigate } from "react-router-dom";
import EditOutlined from '@mui/icons-material/EditOutlined';

const columns = [
  { field: 'id', headerName: 'Student ID', width: 150},
  { field: 'firstName', headerName: 'First name', width: 150 },
  { field: 'lastName', headerName: 'Last name', width: 150 },
  {
    field: "actions",
    headerName: "Actions",
    sortable: false,
    width: 140,
    disableClickEventBubbling: true,
    renderCell: (params) => {
        return (
            <Box sx={{ cursor: "pointer" }}>
                <EditOutlined style={{ fontSize: 16, backgroundColor: "rgb(79 79 79)" }} />
             </Box>
        );
     }
  }
];

export default function StudentsView(props) {
  const { fieldChange, onSubmit, students, formState } = props;
  const navigate = useNavigate();
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const formSubmit = () => {
    handleClose();
    onSubmit();
  };
  return (
    <Box style={{ height: 400,maxWidth: '1000px', margin: 'auto' }}>
      <Typography id="modal-modal-title" variant="h1" component="h1">Students</Typography>
      <Button onClick={handleOpen} variant="outlined" color="success" className="add-btn">Add New Student</Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box className="modal-container">
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Add New Student
          </Typography>
          <Box className="form-container">
            <FormControl>
              <InputLabel htmlFor="my-input">First Name</InputLabel>
              <Input onChange={fieldChange} name="firstName" id="my-input" aria-describedby="my-helper-text" />
            </FormControl>
            <FormControl>
              <InputLabel htmlFor="my-input">Last Name</InputLabel>
              <Input onChange={fieldChange} name="lastName" id="my-input" aria-describedby="my-helper-text" />
            </FormControl>
            <Button disabled={!formState.firstName || !formState.lastName} onClick={formSubmit} className="submit-form" variant="contained">
              Add
            </Button>
          </Box>
        </Box>
      </Modal>
      <DataGrid
        onRowClick={(params)=>navigate(`/students/${params.id}`)}
        style={{"cursor": "pointer"}} 
        rows={students}
        columns={columns}
      />
    </Box>
  );
}