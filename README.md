# Books Library Frontend
Start the project by running the command `npm start`

Important:- You can't delete a Student whom a book is assigned. Delete or remove borrower from that book first.

Demo Video:-
https://www.dropbox.com/s/1mgy4sq5zze5tqd/screen-capture%20%283%29.webm?dl=0
